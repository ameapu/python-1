# python 1
import datetime
now = datetime.datetime.now()
(Giải thích: dùng code lệnh import thời gian, tính thời gian hiện tại now)
print("Current date and time : ")
(Giải thích: lệnh Print đưa ra output của thời gian hiện tại)
Current date and time : 
print (now.strftime("%Y-%m-%d %H:%M:%S"))
(Giải thích: lệnh print đưa ra output thời gian và ngày tại thời điểm hiện tại khi đánh lệnh)
2018-09-15 19:37:25
def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum
(Giải thích: dùng hàm def để cộng hai tham số x và y, Nếu tính tổng trong phạm vi 15 đến 20, trả lại kết quả 20, trả lại hàm sum)
print(sum(10, 6))
print(sum(10, 2))
print(sum(10, 12))
(Giải thích: Tính tổng giá trị trong hàm, 
 đối với giá trị thứ nhất sum (10, 6) tổng là 16 trong phạm vi 15 đến 20 nên trả kết quả là 20
 giá trị thứ 2 sum của 10 và 2 cho kết quả 12, và giá trị thứ 3 tương tự cho kết quả 22. Hai giá trị 12 và 22 thuộc ngoài 
 phạm vi của 15 và 20 nên tính tổng bình thường)
 20
12
22